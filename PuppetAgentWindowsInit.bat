certutil -addstore Root c:\cfn\downloads\GeoTrust_Global_CA.pem
certutil -addstore Root c:\cfn\downloads\cert_0.pem
certutil -addstore CA c:\cfn\downloads\cert_1.pem

cd "C:\Program Files (x86)\Puppet Labs\Puppet\bin\"

@ECHO OFF

puppet module install puppetlabs-windows
