# puppet_lab_2

## Create S3 with CloudFormation:
- Puppet-S3-mokiy.template

## Using aws sync s3 paste all files from local directory to AWS
```aws s3 sync <local-folder> s3://<bucket-name>/<optional-prefix>```

## Paste the next template to CloudFormation:
- Puppet-QuickStart.template

## After infra applied open Linux and Windows agents
Run the following commands inside the system terminals:
- Linux
    - ```sudo puppet agent --test```
- Windows
    - ```puppet_interactive.bat```

## Open the following pages in your local computer
Use:
- http://<public DNS name>/info.php for **Linux**
- http://<public DNS name>/info.aspx for **Windows**
