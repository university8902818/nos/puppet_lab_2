class lampserver {
  package { 'apache2':
    ensure => installed,
  }

  service { 'apache2':
    ensure => running,
  }

  package { 'php5':
    ensure => installed,
  }

  file { '/var/www/html/info.php':
    ensure => file,
    content => '<?php  phpinfo(); ?>',
    require => Package['apache2'],
  }
}
